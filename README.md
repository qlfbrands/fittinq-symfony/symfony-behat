# Symfony Behat Bundle

## Introduction

Welcome to the Symfony Behat Bundle! This powerful tool simplifies the testing of authentication, services, and service mocks in Symfony applications using Behat. This README will guide you through the installation, configuration, and usage of this bundle.

## Table of Contents

- [Installation](#installation)
- [Authenticator Contexts](#authenticator-contexts)
  - [Authenticator Configuration](#authenticator-configuration)
  - [Authenticator Use Cases](#authenticator-use-cases)
- [Services Contexts](#services-contexts)
  - [Service Configuration](#service-configuration)
  - [Service Use Cases](#service-use-cases)
- [Service Mock Contexts](#service-mock-contexts)
  - [Service Mock Configuration](#service-mock-configuration)
  - [Service Mock Use Cases](#service-mock-use-cases)

## Installation

To add this bundle to your Symfony project, simply run the following Composer command:

```bash
composer require fittinq/symfony-behat
```

## Authenticator Contexts

### Authenticator Configuration

To configure your project for authenticator testing, add the Authenticator bundle to your Behat setup. Update your `behat.yml` or `behat.yaml` as follows:

```yaml
default:
   default:
   paths:
      - behat/features
   contexts:
    - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorApiContext
    - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorDatabaseContext
    - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorUserContext
    - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorRoleContext
    - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorFrontendContext
```

### Authenticator Use Cases

#### AuthenticatorRoleContext

The AuthenticatorRoleContext focuses on managing roles and provides steps for adding roles and verifying their existence. Here's an example usage:

```gherkin
Given there are roles
  | name       |
  | ROLE_USER  |
  | ROLE_ADMIN |
  | ROLE_EDITOR|
```

#### AuthenticatorUserContext

This context is designed for managing user-related Behat steps. It includes steps for adding users and authenticating them via an API. Example usage:

```gherkin
Given there are users
  | username | password | roles           |
  | user1    | secret   | ROLE_USER       |
  | admin    | password | ROLE_ADMIN      |
  | editor   | 123456   | ROLE_EDITOR     |
```

#### AuthenticatorFrontendContext

The AuthenticatorFrontendContext is used for testing frontend interactions, such as user logins and checking HTTP status codes and page content. Example usage:

```gherkin
Given user user1 logs in to the frontend app
Then the current page should contain text "Welcome, user1!"
```

## Services Contexts

### Service Configuration

To configure your project for services testing, update your Behat setup as follows:

```yaml
default:
   default:
   paths:
      - behat/features
   contexts:
      - Fittinq\Symfony\Behat\Service\Context\ServiceContext
```

### Service Use Cases

#### Adding Services

You can add services using a Gherkin scenario like this:

```gherkin
Given there are services
   | name     | url                 |
   | service1 | http://service1.com |
   | service2 | http://service2.com |
```

#### Marking Services as Unavailable

Simulate service unavailability with a Gherkin scenario like this:

```gherkin
Given service1 is unavailable
```

## Service Mock Contexts

### Service Mock Configuration

To configure your project for service mocking, update your Behat setup as follows:

```yaml
default:
   default:
   paths:
      - behat/features
   contexts:
      - Fittinq\Symfony\Behat\ServiceMock\Context\ServiceMockContext
```

### Service Mock Use Cases

#### Setting up a Service Response

You can make a service respond with specific data when it is called. Example:

```gherkin
When service1 service responds
"""
{
  "method": "POST",
  "host": "http://service-mock",
  "uri": "/api/testing",
  "headers": [],
  "httpStatusCode": 200,
  "body": {
    "content": "The request was successful"
    }
}
"""
```

#### Checking If a Request Was Made to a Service

You can verify if a request has been made to a service with specific data. Example:

```gherkin
Then a request should have been made to service2
"""
 {
   "method": "POST",
   "host": "http://service-mock",
   "uri": "/api/create",
   "invocationType": "once",
   "headers": [],
   "requests": [
      {
          "headers": {},
          "body": {
            "name": "test"
          }
      }
   ]
  }
"""
```