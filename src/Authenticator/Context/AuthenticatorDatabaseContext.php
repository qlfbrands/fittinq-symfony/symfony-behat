<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Context;

use Behat\Behat\Context\Context;
use Fittinq\Symfony\Behat\Authenticator\Service\DatabaseService;

class AuthenticatorDatabaseContext implements Context
{
    private DatabaseService $databaseService;

    public function __construct(DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    /**
     * @beforeScenario
     */
    public function truncateTables()
    {
        $this->databaseService->truncateUsersAndRoles();
    }
}