<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Fittinq\Symfony\Behat\Authenticator\Service\Role\RoleService;
use PHPUnit\Framework\Assert;

class AuthenticatorRoleContext implements Context
{
    private RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @Given /^there are roles$/
     */
    public function addRoles(TableNode $tableNode)
    {
        foreach ($tableNode as $item) {
            $this->roleService->addRole($item['name']);
        }
    }

    /**
     * @Then /^role (.*) should not exist$/
     */
    public function roleShouldNotExist(string $name)
    {
        Assert::assertFalse($this->roleService->roleExists($name));
    }

    /**
     * @Then /^role (.*) should exist$/
     */
    public function roleShouldExist(string $name)
    {
        Assert::assertTrue($this->roleService->roleExists($name));
    }
}