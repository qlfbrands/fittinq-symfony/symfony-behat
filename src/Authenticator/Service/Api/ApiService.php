<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Service\Api;

use Behat\Gherkin\Node\PyStringNode;
use Fittinq\Symfony\Behat\Authenticator\Service\User\UserService;
use Fittinq\Symfony\Behat\Service\Services\ServicesService;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ApiService
{
    private HttpClientInterface $httpClient;
    private UserService $userService;
    private ResponseInterface $authenticateResponse;
    private ResponseInterface $httpResponse;
    private ServicesService $service;

    public function __construct(HttpClientInterface $httpClient, UserService $userService, ServicesService $service)
    {
        $this->httpClient = $httpClient;
        $this->userService = $userService;
        $this->service = $service;
    }

    public function authenticate(string $username, string $plainPassword): string
    {
        $this->authenticateResponse = $this->httpClient->request(
            'GET',
            'http://dev.auth.hip.fittinq.com/api/v1/authenticate',
            ['auth_basic' => "$username:$plainPassword"]
        );

        $token = $this->authenticateResponse->getContent(false);
        $this->userService->setToken($username, $token);

        return $token;
    }

    public function makeHttpRequest(string $username, string $method, string $service, string $uri, PyStringNode $body = new PyStringNode([], 0)): void
    {
        $user = $this->userService->getUser($username);
        $this->httpResponse = $this->httpClient->request(
            $method,
            $this->service->getUrl($service) . "/" . $uri,
            [
                'auth_bearer' => $this->authenticate(
                    $user ? $user->getUsername() : "",
                    $user ? $user->getPlainPassword() : ""
                ),
                'body' => $body->getRaw()
            ]
        );

        $this->httpResponse->getContent(false);
    }

    public function getHttpResponse(): ResponseInterface
    {
        return $this->httpResponse;
    }

    public function getHttpAuthenticateResponse(): ResponseInterface
    {
        return $this->authenticateResponse;
    }
}
