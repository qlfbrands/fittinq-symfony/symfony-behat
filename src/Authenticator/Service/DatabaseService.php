<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
class DatabaseService
{
    protected Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function insertUser($username, $passwordHashed, $type): int
    {
        $query = "INSERT INTO user (username, password, type) VALUES (:username, :password, :type)";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('username', $username);
        $statement->bindValue('password', $passwordHashed);
        $statement->bindValue('type', $type);
        $statement->executeQuery();

        return (int)$this->connection->lastInsertId();
    }

    public function insertUserRoleRelation(int $userId, int $securityRoleId): int
    {
        $query = "INSERT INTO user_security_role (user_id, security_role_id) VALUES (:userId, :securityRoleId)";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('userId', $userId, ParameterType::INTEGER);
        $statement->bindValue('securityRoleId', $securityRoleId, ParameterType::INTEGER);
        $statement->executeQuery();

        return (int)$this->connection->lastInsertId();
    }

    public function insertRole(string $name)
    {
        $query = "INSERT INTO security_role (name) VALUES (:name)";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('name', $name);
        $statement->executeQuery();

        return (int) $this->connection->lastInsertId();
    }

    public function getIdBySecurityRoleName($securityRoleName): ?int
    {
        $query = "SELECT id FROM security_role WHERE name = :securityRoleName";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('securityRoleName', $securityRoleName);
        $result = $statement->executeQuery();
        $data = $result->fetchAllAssociative();

        if (empty($data)) {
            return null;
        }

        return $data[0]['id'];
    }

    public function truncateUsersAndRoles()
    {
        $statement = $this->connection->prepare(
    "SET FOREIGN_KEY_CHECKS = 0;" .
        "TRUNCATE TABLE user_security_role;" .
        "TRUNCATE TABLE security_role;" .
        "TRUNCATE TABLE user;" .
        "SET FOREIGN_KEY_CHECKS = 1;"
        );

        $statement->executeQuery();
    }
}