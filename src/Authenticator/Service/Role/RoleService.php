<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Service\Role;

use Fittinq\Symfony\Behat\Authenticator\Service\DatabaseService;

class RoleService
{
    private DatabaseService $databaseService;
    /**
     * @var RoleData[]
     */
    private array $roles;

    public function __construct(DatabaseService $databaseService)
    {
        $this->databaseService = $databaseService;
    }

    public function addRole(string $name): void
    {
      $id = $this->databaseService->insertRole($name);

      $role = new RoleData();
      $role->setId($id);
      $role->setName($name);

      $this->roles[] = $role;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getRoleByName(string $name): ?RoleData
    {
        foreach ($this->roles as $role){
            if ($name === $role->getName()){
                return $role;
            }
        }

        return null;
    }

    public function roleExists(string $roleName): bool
    {
       return $this->getRoleByName($roleName) !== null;
    }


}