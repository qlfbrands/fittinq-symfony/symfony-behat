<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\BasicAuthenticator\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Fittinq\Symfony\Behat\Service\Services\ServicesService;
use PHPUnit\Framework\Assert;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class BasicAuthenticatorContext implements Context
{
    private HttpClientInterface $httpClient;
    private ServicesService $serviceProvider;
    private ResponseInterface $response;

    public function __construct(HttpClientInterface $httpClient, ServicesService $serviceProvider)
    {
        $this->httpClient = $httpClient;
        $this->serviceProvider = $serviceProvider;
    }

    /**
     * @When /^(.*) user makes a (.*) request to ([^\/]*)\/(.*)$/
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function makeAuthenticatedRequest(PyStringNode $string, string $kindOfUser, string $method, string $service, string $uri): void
    {
        if ($kindOfUser === 'authorized') {
            $baseEncodedUsernamePassword = base64_encode("{$_ENV['BASIC_AUTH_USERNAME']}:{$_ENV['BASIC_AUTH_PASSWORD']}");
        }else{
            $baseEncodedUsernamePassword = base64_encode("{$_ENV['BASIC_AUTH_USERNAME']}:wrongPassword");
        }

        $this->makeRequest($string, $service, $method, $uri, $baseEncodedUsernamePassword);

        $this->response->getContent(false);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function makeRequest(
        PyStringNode $string,
        string $service,
        string $method,
        string $uri,
        string $baseEncodedUsernamePassword
    ): void {
        $body = $string->getRaw();

        $url = $this->serviceProvider->getUrl($service);

        $this->response = $this->httpClient->request(
            $method,
            "$url/$uri",
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Basic ' . $baseEncodedUsernamePassword,
                ],
                'body' => $body
            ]
        );
    }

    /**
     * @Given /^the controller should return a (\d+) response status code$/
     * @throws TransportExceptionInterface
     */
    public function theControllerShouldReturnAResponseStatusCode(int $statusCode): void
    {
        Assert::assertEquals($statusCode, $this->response->getStatusCode());
    }
}