<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Command\Assert;

use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Symfony\Component\Process\Process;

class AssertErrorLogged extends Waiter
{
    private string $expectedOutput;
    private Process $process;

    public function __construct(string $expectedOutput, Process $process)
    {
        $this->expectedOutput = $expectedOutput;
        $this->process = $process;
    }

    protected function play(): bool
    {
        if (!empty($this->process->getOutput())){
            return true;
        }

        return false;
    }

    protected function onAfterPlay(): void
    {
        Assert::assertStringContainsString($this->expectedOutput, $this->process->getOutput());
    }
}