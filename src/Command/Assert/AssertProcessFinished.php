<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Command\Assert;

use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Process\Process;

class AssertProcessFinished extends Waiter
{
    private Process $process;

    public function __construct(Process $process)
    {
        $this->process = $process;
    }

    protected function play(): bool
    {
        return $this->process->getStatus() == Process::STATUS_TERMINATED;
    }

    protected function onAfterPlay(): void
    {
        Assert::assertEquals(Command::SUCCESS, $this->process->getExitCode());
        Assert::assertEquals(PROCESS::STATUS_TERMINATED, $this->process->getStatus());
    }
}