<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Command\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Fittinq\Symfony\Behat\Command\Assert\AssertErrorLogged;
use Fittinq\Symfony\Behat\Command\Assert\AssertProcessFinished;
use Fittinq\Symfony\Behat\Command\Source\Command;
use Symfony\Component\Process\Process;

class ProcessContext implements Context
{
    /**
     * @var Process[]
     */
    private array $processes = [];
    private Command $command;

    public function __construct(Command $command)
    {
        $this->command = $command;
    }

    /**
     * @Given /^the (.*) process is running$/
     */
    public function startProcess(string $command): void
    {
        $process = $this->command->startProcess($command);
        $this->processes[$command] = $process;
    }

    /**
     * @Then /^assert that the (.*) process is finished/
     */
    public function processFinished(string $command): void
    {
        $process = $this->processes[$command];

        $assert = new AssertProcessFinished($process);
        $assert->wait();
    }

    /**
     * @Then /^assert that a message is logged running command (.*)$/
     */
    public function assertThatAMessageIsLogged(PyStringNode $strings, string $command): void
    {
        $process = $this->processes[$command];
        $assert = new AssertErrorLogged($strings->getStrings()[0], $process);
        $assert->wait();
    }

    /**
     * @afterScenario
     */
    public function stopProcess(): void
    {
        foreach ($this->processes as $process) {
            $this->command->stopProcess($process);
        }
    }
}
