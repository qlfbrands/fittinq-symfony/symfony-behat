<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Command\Source;

use Symfony\Component\Process\Process;

class Command
{
    public function startProcess(string $command): Process
    {
        $process = new Process(["bin/console", $command]);
        $process->start();

        return $process;
    }

    public function stopProcess(Process $process): void
    {
        $process->stop();
    }
}
