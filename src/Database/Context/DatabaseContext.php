<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Database\Context;

use Behat\Behat\Context\Context;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManagerInterface;

class DatabaseContext implements Context
{
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @beforeScenario
     * @throws Exception
     */
    public function removeAll(): void
    {
        if (getenv('APP_ENV') === 'behat') {
            $connection = $this->entityManager->getConnection();
            $platform = $connection->getDatabasePlatform();
            $schemaManager = $connection->createSchemaManager();

            $this->truncateAllTables($connection, $schemaManager, $platform);
        }
    }

    /**
     * @throws Exception
     */
    public function truncateAllTables(
        Connection $connection,
        AbstractSchemaManager $schemaManager,
        ?AbstractPlatform $platform
    ): void
    {
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS=0');

        foreach ($schemaManager->listTableNames() as $table) {
            $connection->executeQuery($platform->getTruncateTableSQL($table, true));
        }

        $connection->executeQuery('SET FOREIGN_KEY_CHECKS=1');
    }
}