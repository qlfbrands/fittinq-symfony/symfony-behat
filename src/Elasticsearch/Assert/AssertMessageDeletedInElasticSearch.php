<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Elasticsearch\Assert;

use Elasticsearch\Client;
use PHPUnit\Framework\Assert;
use Fittinq\Symfony\Behat\Waiter\Waiter;

class AssertMessageDeletedInElasticSearch extends Waiter
{
    private array $actualMessage = [];
    private Client $client;
    private string $index;
    private string $query;

    public function __construct(Client $client, string $index, string $query)
    {
        $this->client = $client;
        $this->index = $index;
        $this->query = $query;
    }

    protected function play(): bool
    {
        $this->actualMessage = $this->getMessage();

        if (!empty($this->actualMessage)) {
            return false;
        }

        return true;
    }

    private function getMessage(): array
    {
        $elasticsearchClient = new ElasticSearchQuery($this->client, $this->index, $this->query);

        return $elasticsearchClient->getByStringQuery();
    }

    protected function onAfterPlay(): void
    {
        Assert::assertEmpty($this->actualMessage, json_encode($this->actualMessage));
    }
}
