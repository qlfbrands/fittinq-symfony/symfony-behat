<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Elasticsearch\Assert;

use Elasticsearch\Client;
use PHPUnit\Framework\Assert;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use stdClass;
use Throwable;

class AssertMessageLoggedInElasticSearch extends Waiter
{
    private stdClass $expectedMessage;
    private array $actualMessage = [];
    private string $query;
    private Client $client;
    private string $index;

    public function __construct(stdClass $message, string $query, Client $client, string $index)
    {
        $this->expectedMessage = $message;
        $this->query = $query;
        $this->client = $client;
        $this->index = $index;
    }

    protected function play(): bool
    {
        /* todo This try catch is here because some of these requests fail with a no_shard_available_action_exception
         * We think this is is due to the fact that no index has yet been created.
         */
        try {
            if (!empty($this->getMessage())) {
                $this->actualMessage = $this->getMessage();
                return true;
            }
        } catch (Throwable) {
        }

        return false;
    }

    private function getMessage(): array
    {
        $elasticsearchClient = new ElasticSearchQuery($this->client, $this->index, $this->query);

        return $elasticsearchClient->getByStringQuery();
    }

    protected function onAfterPlay(): void
    {
        Assert::assertNotEmpty($this->actualMessage);
        Assert::assertEquals($this->expectedMessage->level, $this->actualMessage[0]['_source']['level']);
        Assert::assertMatchesRegularExpression("/{$this->expectedMessage->createdAt}/", strval($this->actualMessage[0]['_source']['createdAt']));
        Assert::assertStringContainsString($this->expectedMessage->message, $this->actualMessage[0]['_source']['message']);
        Assert::assertMatchesRegularExpression($this->expectedMessage->context->message_id, $this->actualMessage[0]['_source']['context']['message_id']);
        Assert::assertEquals($this->expectedMessage->context->queue, $this->actualMessage[0]['_source']['context']['queue']);
    }
}
