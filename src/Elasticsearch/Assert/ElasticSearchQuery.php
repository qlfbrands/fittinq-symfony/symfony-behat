<?php

namespace Fittinq\Symfony\Behat\Elasticsearch\Assert;

use Elasticsearch\Client;

class ElasticSearchQuery
{
    private Client $client;
    private string $index;
    private string $query;

    public function __construct(Client $client, string $index, string $query)
    {
        $this->client = $client;
        $this->index = $index;
        $this->query = $query;
    }

    public function getByStringQuery(): array
    {
        $queryDSL = [
            'query' => [
                'query_string' => [
                    'query' => $this->query
                ]
            ]
        ];

        $results = $this->client->search([
            'index' => $this->index . '*',
            'body' => $queryDSL
        ]);

        return $results['hits']['hits'];
    }
}