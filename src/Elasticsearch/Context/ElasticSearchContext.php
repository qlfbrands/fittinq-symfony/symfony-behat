<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Elasticsearch\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Elasticsearch\Client;
use Fittinq\Logger\Elastic\ClientBuilder;
use Fittinq\Symfony\Behat\Elasticsearch\Assert\AssertMessageDeletedInElasticSearch;
use Fittinq\Symfony\Behat\Elasticsearch\Assert\AssertMessageInElasticSearch;
use Fittinq\Symfony\Behat\Elasticsearch\Assert\AssertMessageLoggedInElasticSearch;
use stdClass;

class ElasticSearchContext implements Context
{
    private Client $client;
    private string $errorIndex;
    private string $archiverIndex;
    private string $deadLetterIndex;
    private string $appEnv;

    public function __construct(
        ClientBuilder $builder,
        string $errorIndex,
        string $archiverIndex,
        string $deadLetterIndex,
        string $appEnv
    )
    {
        $this->client = $builder->getClient();
        $this->errorIndex = $errorIndex;
        $this->archiverIndex = $archiverIndex;
        $this->deadLetterIndex = $deadLetterIndex;
        $this->appEnv = $appEnv;
    }

    /**
     * @Given /^there are dead\-lettered messages$/
     */
    public function saveDeadLetteredMessages(TableNode $table): void
    {
        $this->saveMessages($this->deadLetterIndex, $table);
    }

    /**
     * @Given /^there are archived messages$/
     */
    public function saveArchivedMessages(TableNode $table): void
    {
        $this->saveMessages($this->archiverIndex, $table);
    }

    /**
     * @Given /^there are error messages$/
     */
    public function saveAmqErrorMessages(TableNode $table): void
    {
        $this->saveMessages($this->errorIndex, $table);
    }

    /**
     * @Then /^the message should be dead lettered/
     */
    public function assertMessageIsDeadLettered(PyStringNode $message): void
    {
        $this->assertMessageInElasticSearch($this->deadLetterIndex, json_decode($message->getRaw()));
    }

    /**
     * @Then /^the message should be archived$/
     */
    public function assertMessageIasArchived(PyStringNode $message): void
    {
        $this->assertMessageInElasticSearch($this->archiverIndex, json_decode($message->getRaw()));
    }

    /**
     * @Then /^the message should be logged in elasticsearch$/
     */
    public function assertMessageIsLogged(PyStringNode $message): void
    {
        $this->assertMessageLoggedInElasticSearch($this->errorIndex, json_decode($message->getRaw()));
    }

    /**
     * @Then /^the message?s? should be deleted in Elasticsearch$/
     */
    public function assertMessagesDeleted(PyStringNode $messages): void
    {
        foreach (json_decode($messages->getRaw()) as $message){
            $assert = new AssertMessageDeletedInElasticSearch($this->client, $message->index, $message->id);
            $assert->wait();
        }
    }

    private function saveMessages(string $index, TableNode $table): void
    {
        $index .= "-$this->appEnv-" . date('Ymd');

        foreach ($table as $key => $item) {
            $this->saveInElastic($index, $item);
            if ($key == array_key_last(array($table))) {
                $this->assertDataExists($index, $item);
            }
        }
    }

    private function saveErrorMessages(string $index, TableNode $table): void
    {
        $index .= "-$this->appEnv-" . date('Ymd');

        foreach ($table as $key => $item) {
            $this->saveInElastic($index, $item);
            if ($key == array_key_last(array($table))) {
                $this->assertDataExists($index, $item);
            }
        }
    }

    private function saveInElastic(string $index, array $item): void
    {
        $index = $this->createIndexIfNotExists($index);
        if (str_contains($index, 'qlf-amq-error')){
            $this->addToLoggerIndex($index, $item);
        }else{
            $this->addToIndex($index, $item);
        }
    }

    private function createIndexIfNotExists(string $index): string
    {
        $indexExists = $this->client->indices()->exists(['index' => $index]);

        if (!$indexExists) {
            $this->createIndex($index);
        }

        return $index;
    }

    private function createIndex(string $index): void
    {
        $indexParams = [
            'index' => $index,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0,
                ],
                'mappings' => [
                    'properties' => [
                        'createdAt' => [
                            'type' => 'date',
                            'format' => 'epoch_millis',
                        ],
                    ],
                ],
            ],
        ];

        $this->client->indices()->create($indexParams);
    }

    private function addToIndex(string $index, array $item): void
    {
        $createdAt = round(microtime(true) * 1000);
        $this->client->index(
            [
                'index' => $index,
                'body' => [
                    'id' => $item['id'],
                    'createdAt' => $createdAt,
                    'exchange' => $item['exchange'],
                    'routingKey' => '',
                    'queue' => $item['queue'],
                    'body' => $item['body'],
                ],
            ]
        );
    }

    private function addToLoggerIndex(string $index, array $item): void
    {
        $createdAt = round(microtime(true) * 1000);
        $this->client->index(
            [
                'index' => $index,
                'body' => [
                    'id' => $item['id'],
                    'level' => $item['level'],
                    'createdAt' => $createdAt,
                    'message' => $item['message'],
                    'context' => [
                        "message_id" => $item['deadLetterId'],
                        "queue" => $item['queue']
                    ]
                ],
            ]
        );
    }

    private function assertDataExists(string $index, array $item): void
    {
        if (str_contains($index, 'qlf-amq-error')){
            $this->assertAmqErrorExists($item, $index);
        }else{
            $this->assertMessageArchived($item, $index);
        }
    }

    private function assertMessageInElasticSearch(string $index, stdClass $message): void
    {
        $assert = new AssertMessageInElasticSearch(
            $message,
            $message->exchange,
            $this->client,
            $index
        );

        $assert->wait();
    }

    public function assertAmqErrorExists(array $item, string $index): void
    {
        $context = new stdClass();
        $context->message_id = "/^[a-f0-9]{13}$/";
        $context->queue = $item['queue'];

        $body = new stdClass();
        $body->level = $item['level'];
        $body->createdAt = "\\d*";
        $body->message = $item['message'];
        $body->context = $context;

        $this->assertMessageLoggedInElasticSearch($index, $body);
    }

    public function assertMessageArchived(array $item, string $index): void
    {
        $body = new stdClass();
        $body->id = $item['id'];
        $body->createdAt = "\\d*";
        $body->exchange = $item['exchange'];
        $body->queue = $item['queue'];
        $body->routingKey = '';
        $body->body = json_decode($item['body']);

        $this->assertMessageInElasticSearch($index, $body);
    }

    private function assertMessageLoggedInElasticSearch(string $index, stdClass $message): void
    {
        $assert = new AssertMessageLoggedInElasticSearch(
            $message,
            '*',
            $this->client,
            $index
        );

        $assert->wait();
    }

    /**
     * @afterScenario
     */
    public function deleteIndices(): void
    {
        $this->client->indices()->delete(['index' => $this->errorIndex . '-dev*']);
        $this->client->indices()->delete(['index' => $this->errorIndex . '-behat*']);
        $this->client->indices()->delete(['index' => 'qlf-hip-behat*']);
        $this->client->indices()->delete(['index' => 'qlf-hip-dev*']);
        $this->client->indices()->delete(['index' => $this->archiverIndex . '-dev*']);
        $this->client->indices()->delete(['index' => $this->archiverIndex . '-behat*']);
        $this->client->indices()->delete(['index' => $this->deadLetterIndex . '-behat*']);
        $this->client->indices()->delete(['index' => $this->deadLetterIndex . '-dev*']);
    }
}
