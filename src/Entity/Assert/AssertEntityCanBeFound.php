<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Entity\Assert;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;

class AssertEntityCanBeFound extends Waiter
{
    private array $search;
    private ServiceEntityRepository $repository;
    private ?object $entity;

    public function __construct(ServiceEntityRepository $repository, array $search)
    {
        $this->search = $search;
        $this->repository = $repository;
    }

    /**
     * @return bool
     */
    protected function play(): bool
    {
        $this->entity = $this->repository->findOneBy($this->search);

        if (null === $this->entity) {
            return false;
        }else{
            return true;
        }
    }

    protected function onAfterPlay(): void {
        Assert::assertNotNull($this->entity, 'Entity is not updated');
    }
}