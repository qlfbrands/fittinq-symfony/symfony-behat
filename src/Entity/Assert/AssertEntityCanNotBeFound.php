<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Entity\Assert;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;

class AssertEntityCanNotBeFound extends Waiter
{
    private array $search;
    private ObjectRepository $repository;
    private ?object $entity;
    private bool $found = false;

    public function __construct(ObjectRepository $repository, array $search)
    {
        $this->search = $search;
        $this->repository = $repository;
    }

    /**
     * @return bool
     */
    protected function play(): bool
    {
        $this->entity = $this->repository->findOneBy($this->search);

        if (null === $this->entity) {
            return $this->found = false;
        }else{
            return $this->found = true;
        }
    }

    protected function onAfterPlay(): void {
        Assert::assertFalse($this->found, 'Entity is found');
    }
}