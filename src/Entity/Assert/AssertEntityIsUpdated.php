<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Entity\Assert;

use Doctrine\ORM\EntityManagerInterface;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;

class AssertEntityIsUpdated extends Waiter
{
    private EntityManagerInterface $entityManager;
    private mixed $entity;
    private bool $updated;
    /**
     * @var callable
     */
    private $callback;

    public function __construct(EntityManagerInterface $entityManager, mixed $entity, callable $callback)
    {
        $this->entityManager = $entityManager;
        $this->entity = $entity;
        $this->callback = $callback;
    }

    protected function play(): bool
    {
        try {
            $this->entityManager->refresh($this->entity);
            $this->updated = ($this->callback)($this->entity);
            return $this->updated;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function onAfterPlay(): void {
        Assert::assertTrue($this->updated, 'Entity is not updated');
    }
}
