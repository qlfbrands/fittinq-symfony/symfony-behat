<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Service\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Fittinq\Symfony\Behat\Service\Services\ServicesService;

class ServiceContext implements Context
{
    /**
     * @var ServicesService
     */
    private ServicesService $serviceService;
    private array $variables = [];


    public function __construct(ServicesService $serviceService)
    {
        $this->serviceService = $serviceService;
    }


    /**
     * @Given /^there are services$/
     */
    public function addUsers(TableNode $tableNode)
    {
        foreach ($tableNode as $item) {
            $this->serviceService->addService($item['name'], $item['url']);
        }
    }

    /**
     * @Given /^(.*) is unavailable$/
     */
    public function setServiceUnavailable(string $serviceName)
    {
        $envName = strtoupper($serviceName) . "_HOST_URL";
        $env = file_get_contents('.env.local');
        preg_match("/$envName=(.*)/", $env, $matches);
        $envValue = $matches[1];

        $this->variables[$envName] = $envValue;

        exec("sed -i 's/$envName=.*/$envName=not_available/g' .env.local");
        exec("bin/console cache:clear");
    }

    /**
     * @AfterScenario
     */
    public function setUnavailableServicesAsAvailable()
    {
        foreach ($this->variables as $envName => $envValue) {
            $envValue = addcslashes($envValue, '/');
            exec("sed -i 's/$envName=not_available/$envName=$envValue/g' .env.local");
        }

        exec("bin/console cache:clear");
    }
}