<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Service\Services;

class ServicesService
{
    /**
     * @var string[]
     */
    private array $services = [];

    public function addService(string $name, string $url): void
    {
        $this->services[$name] = $url;
    }

    public function getUrl(string $name): ?string
    {
        return $this->services[$name];
    }
}
