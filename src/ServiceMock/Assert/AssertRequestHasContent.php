<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\ServiceMock\Assert;

use Behat\Gherkin\Node\PyStringNode;
use Fittinq\Symfony\Behat\Waiter\Waiter;
use PHPUnit\Framework\Assert;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AssertRequestHasContent extends Waiter
{
    private string $method;
    private HttpClientInterface $httpClient;
    private string $url;
    protected string $body;
    private bool $hasContent = false;
    private string $message = '';


    public function __construct(string $method, HttpClientInterface $httpClient, string $url, string $body)
    {
        $this->method = $method;
        $this->httpClient = $httpClient;
        $this->url = $url;
        $this->body = $body;
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function play(): bool
    {
        try {
            $response = $this->httpClient->request(
                $this->method,
                $this->url,
                ["body" => $this->body]
            );

            $response->getContent();
            $this->hasContent = true;
            return true;
        } catch (ClientExceptionInterface $exception){
            $this->handleCatch($exception);
            $this->message = $exception->getResponse()->getContent(false);
            return false;
        }
    }

    protected function handleCatch(ClientExceptionInterface $exception): void {}

    protected function onAfterPlay(): void {
        Assert::assertTrue($this->hasContent, $this->message);
    }
}
