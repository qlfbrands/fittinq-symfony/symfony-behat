<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\ServiceMock\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeStepScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\StepNode;
use Fittinq\Symfony\Behat\Service\Services\ServicesService;
use Fittinq\Symfony\Behat\ServiceMock\Assert\AssertRequestHasContent;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServiceMockContext implements Context
{
    private ServicesService $servicesService;
    private HttpClientInterface $httpClient;
    private ?StepNode $lastStep = null;

    public function __construct(ServicesService $servicesService, HttpClientInterface $httpClient)
    {
        $this->servicesService = $servicesService;
        $this->httpClient = $httpClient;
    }

    /**
     * @Given /^(.*) service responds$/
     */
    public function setServiceResponse(string $service, PyStringNode $string): void
    {
        $assert = new AssertRequestHasContent(
            'POST',
            $this->httpClient,
            $this->servicesService->getUrl($service) . '/endpoints/responses',
            $string->getRaw()
        );

        $assert->wait();
    }

    /**
     * @Then /^a message should have been sent to (.*)/
     */
    public function assertRequest(string $service, PyStringNode $string): void
    {
        $requestContent = $this->replaceEnvVariables($string->getRaw());

        $request = json_decode($requestContent);
        // If the invocation type is 'never', wait for 2 seconds, as the service-mock database may not be updated yet.
        if ($request->invocationType === 'never'){
            sleep(2);
        }

        $assert = new AssertRequestHasContent(
            'POST',
            $this->httpClient,
            $this->servicesService->getUrl($service) . '/endpoints/validate',
            $requestContent
        );

        $assert->wait();
    }

    private function replaceEnvVariables(string $content): string
    {
        $env = file_get_contents('.env.local');
        // Match %env('...')% placeholders and replace them with the actual values
        $pattern = '/%env\(\'(.*?)\'\)%/';
        preg_match_all($pattern, $content, $matches);

        foreach ($matches[0] as $index => $placeholder) {
            $envVariable = $this->readEnvVariable($matches[1][$index], $env);

            if ($envVariable !== '') {
                $content = str_replace($placeholder, $envVariable, $content);
            }
        }

        return $content;
    }

    private function readEnvVariable($envName, $env):string{
        preg_match("/$envName=(.*)/", $env, $matches);
        return $matches[1];
    }


    /**
     * @BeforeStep
     */
    public function beforeStep(BeforeStepScope $scope): void
    {
        $currentStep = $scope->getStep();

        if ($this->lastStep === null || $this->lastStep->getKeywordType() === 'Given') {
            if ($currentStep->getKeywordType() !== 'Given') {
                $this->httpClient->request('DELETE', "http://service-mock/endpoints");
            }
        }

        $this->lastStep = $currentStep;
    }
}
