<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyBehatBundle extends Bundle
{
}