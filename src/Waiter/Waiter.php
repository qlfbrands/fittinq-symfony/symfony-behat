<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Waiter;

abstract class Waiter
{
    const MAX_WAITING_TIME = 6;

    public function wait(): void
    {
        $start = microtime(true);

        while (true) {
            $now = microtime(true);

            $shouldStop = $this->play();

            if ($shouldStop || $now - $start > self::MAX_WAITING_TIME) {
                $this->onAfterPlay();
                return;
            }

            usleep(100000);
        }
    }

    abstract protected function play(): bool;

    abstract protected function onAfterPlay(): void;
}